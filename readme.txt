2013--2014 Johan S. R. Nielsen

Scripts for automatic fixing of BibTeX files.

This repository contains the following tools for simple, automated fixes in
BibTeX files:

- abbrev.pl: Abbreviate journal, conference, etc. names in BibTeX files.
- freshen.pl: Various small fixes: protect capitalisation, optionally remove
  unwanted properties on entries.

All scripts follow the same general format: input some options as well as a
bibtex file, and output the new bibtex file on standard output.

If the input bibtex file is omitted or specified as "-", then it is read from
standard input.
This allows easy chaining such as
 cat orig-bibtex.bib | ./abbrev.pl abbrevs.txt
                     | ./freshen -UD protections.txt > new-bibtex.bib


LIMITATIONS:
The BibTeX file is not parsed; it is simply pattern matched, line by line, so
the BibTeX file is assumed to be quite regular and nice. For instance, for
journal, etc. lines to be registered by abbrev.pl, they need to have the
following structure:
  <type> = "<title>",
where <type> is either journal or booktitle (lower case), and where any single
space can be replaced by any number of spaces. The comma at the end is not
mandatory, though.
Similar limitations pertain to all tools.

This program is in the public domain. You may use it for anything, reproduce
it, delete my# name from it, modify it or even attempt to make money with it if
you like.



Usage of abbrev.pl:
--------------------------------------------------------------------------------
  ./abbrev.pl  <abbrev-file> <bibtex-file>
Runs through a BibTeX file and replaces all journal, conference, series,
publisher and booktitle entries with the mapping defined by an
abbreviation-file. For any such entry not specified in the abbreviation file,
an error is output.

An abbreviation file is a line-by-line specification of the form
<full name>=<abbreviated>
If <abbreviated> is omitted then no abbreviation is performed.
All lines beginning with # are ignored. 





Usage of freshen.pl:
--------------------------------------------------------------------------------
  ./freshen.pl [-U] [-D] <protected-words-file> <bibtex-file>
Runs through a BibTeX file and "freshens it up":
- Capitalisation of titles is protected: if capitalisation file is "-" then
  simply protect all titles completely. Otherwise, protect only the capital
  letters of the words listed line-by-line in the <protected-words-file>.
- Optional flag -U means to remove URL properties from all entries.
  This can be useful if the BibTeX is generated from a bibliography-tool such
  as Zotero, but one does not wish to put the full URL in the references of
  e.g. a short conference paper.
  urldate properties are also removed.
- Optional flag -D means to remove DOI properties from all entries.
