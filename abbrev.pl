#!/usr/bin/env perl
# 2013 Johan S. R. Nielsen
#
# Abbreviate journal, conference, series and publisher names in BibTeX files.
#
# Usage:
#   ./abbrev.pl  <abbrev-file> <bibtex-file>
# Runs through a BibTeX file and replaces all journal and booktitle entries with
# the mapping defined by an abbreviation-file.
# If the input bibtex is "-" then input is read from standard input.
# The new BibTeX file is output to standard output
# See also readme.txt 
#
# LIMITATIONS:
# The BibTeX file is not parsed; it is simply pattern matched, line by line. In
# particular, for journal and booktitle lines to be discovered, they need to
# have the following structure:
#   <type> = "<title>",
# where <type> is one of journal, booktitle, series or publisher (lower case),
# and where any single space can be replaced by any number of spaces. The comma
# at the end is not mandatory, though.
#
# This file is in the public domain. You may use it for anything, reproduce it,
# delete my name from it, modify it or even attempt to make money with it if you
# like.

use strict;
use warnings;

die "Usage:\n   ./abbrev.pl <abbrev-file> <bib-tex-file>" if (@ARGV != 2 && @ARGV != 1);

#Read the journal abbreviation file
my %abbrevs; # shall map a journal name to its abbreviation

my $abbrname = shift @ARGV;
open(my $abbrfile,"<", $abbrname) or die "Can't open $abbrname: $!'";
while (<$abbrfile>) {
    my $a = $_;
    unless ($a =~ /^#.*|^\s*$/) {
        if ($a =~ /(.*)=(.*)/) {
            if ($2 eq "") {
                $abbrevs{$1} = $1;
            } else {
                $abbrevs{$1} = $2;
            }
        } else {
            print "Possibly corrupted abbreviations file: $_\n";
        }
    }
}
close $abbrfile;

my $out_file = *STDOUT;
#Read all lines and match for journal/booktitle entries
while (<>) {
    if (/^(\s*)(journal|booktitle|series|publisher)\s*=\s*{(.*)},?\s*$/) {
        my $space=$1; my $type = $2;
        my $title = $3;
        $title =~ s/\{([^{}]*)\}/$1/g;
        if (defined $abbrevs{$title}) {
            $title= $abbrevs{$title};
        } else {
            print STDERR "Unknown $type: '$title'\n";
        }
        print $out_file "$space$type = {$title},\n";
    } else {
        print $out_file "$_";
    }
}
close $out_file;
