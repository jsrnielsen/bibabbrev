#!/usr/bin/perl
# 2014 Johan S. R. Nielsen
#
# Freshen up a BibTex file by doing a list of stuff to it:
# - Protect capitalisation in titles by protecting entire title or using a list of predefined words
# - Optionally remove URL on all entries
# - Optionally remove DOI on all entries
#
# Usage:
#   ./freshen.pl [-A] [-U] [-D]  <protected-words-file> <bibtex-file>
# Runs through a BibTeX file and does its stuff.
# If capitalisation file is "-" then simply protect all titles completely.
# Optional flag -A means to remove address.
# Optional flag -U means to remove URL.
# Optional flag -D means to remove DOI.
# The new BibTeX file is output to standard output
# See also readme.txt 
#
# LIMITATIONS:
# The BibTeX file is not parsed; it is simply pattern matched, line by line. In
# particular, for the respective lines to be discovered, they need to
# have the following structure:
#   <type> = "<title>",
# where <type> is one of title, url, or doi (lower case),
# and where any single space can be replaced by any number of spaces. The comma
# at the end is also not mandatory.
#
# This file is in the public domain. You may use it for anything, reproduce it,
# delete my name from it, modify it or even attempt to make money with it if you
# like.

use strict;
use warnings;
use Getopt::Std;

my %args;
getopts('AUD', \%args);
my $strip_addr = $args{'A'} || 0;
my $strip_doi = $args{'D'} || 0;
my $strip_url = $args{'U'} || 0;

#die "Usage:\n   ./freshen.pl [-A] [-U] [-D] <protected-word-file> <bib-tex-file>" if @ARGV < 2;

#Read the word protection file
my $pfile = shift @ARGV;
my $protect_all = $pfile eq "-";
my %prots;
if (!$protect_all) {
    open my $prots_file, "<", $pfile or die "Can't open protection list file: $!";
    while(my $line = <$prots_file>) {
        $line =~ s/^\s*([^\s]*)\s*$/$1/;
        $prots{$line} = ();
    }
}

my $out_file = *STDOUT;

sub protect {
    my $word = shift @_;
    if (exists $prots{$word}) {
        $word =~ s/([A-Z])/{$1}/g;
        return  $word;
    } else {
        return $word;
    }
}
#Read all lines and match for titles
while (my $line = <>) {
    if ($line =~ /^(\s*)title\s*=\s*{(.*)},?\s*$/) {
        my $protected = $protect_all ? "{$2}" : join('',map { protect($_) } split(/( |-|–|')/,$2));
        print $out_file "$1title = {$protected},\n";
    } elsif ( ($strip_doi && $line =~ /^(\s*)doi\s*=/)
              ||($strip_addr && $line =~ /^(\s*)address\s*=/)
              ||($strip_url && $line =~ /^(\s*)url\s*=/)
              ||($strip_url && $line =~ /^(\s*)urldate\s*=/) ) {
        # don't print the line
    } else {
        print $out_file $line;
    }
}
close $out_file;
