#!/usr/bin/env perl
# Extract from the IEEE files IEEEfull.bib and IEEEabbr.bib, a list of full names to abbreviations.
use strict;
use warnings;

die "Usage:\n   ./extract_ieee.pl <IEEEfull.bib> <IEEEabbr.bib>" if @ARGV != 2;

#Read the files
my %fulls;   # shall map bibtex key to its full name
my %abbrevs;

open(my $fullfile,"<", $ARGV[0]) or die "Can't open $ARGV[0]: $!'";
while (<$fullfile>) {
    if (/\@STRING{(.*)\s*=\s*"(.*)"}\s*/) {
        $fulls{$1} = $2;
    }
}
close $fullfile;
open(my $abbrfile,"<", $ARGV[1]) or die "Can't open $ARGV[1]: $!'";
while (<$abbrfile>) {
    if (/\@STRING{(.*)\s*=\s*"(.*)"}\s*/) {
        $abbrevs{$1} = $2;
    }
}
close $abbrfile;

my @keys = keys %fulls;
foreach my $key (@keys) {
    if (defined $abbrevs{$key}) {
        print "$fulls{$key}=$abbrevs{$key}\n";
    } else {
        print STDERR "Key undefined in abbreviation: $key\n";
    }
}
